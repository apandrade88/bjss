import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class PriceBasket {

	static JsonArray products;
	static JsonArray offers;

	public static void main(String[] args) {
		loadProducts();
		loadOffers();
		// group basket Items. Ex.: Milk=3, Soup=2
		Map<String, Long> basket = Arrays.stream(args).collect(
                Collectors.groupingBy(
                        Function.identity(), Collectors.counting()
                )
        );
		
		DoubleAccumulator subtotal = new DoubleAccumulator(Double::sum,0L);
		DoubleAccumulator total    = new DoubleAccumulator(Double::sum,0L);
		StringBuilder     offers   = new StringBuilder();
		basket.forEach((productName, quantity) ->{
				
			JsonObject productBuy = getProduct(productName);
			if(productBuy == null) {
				System.out.println(productName+": Prduct not found");
			}else
			{
				subtotal.accumulate(productBuy.getJsonNumber("price").doubleValue() * quantity); // calculates the total value per product

				List<JsonObject> filteredOffers  = filterOffers(productName); // find available product offers
				filteredOffers.forEach(offer -> {
					Long productInBasket = basket.get(offer.getString("get"));	
					if(productInBasket != null) // check if the product is receiving the discount is in the basket
					{
						int        offerGranted   = quantity.intValue() / offer.getInt("quantity"); //calculates how many offers were granted
						int        basketQuantity = productInBasket.intValue();
						
						if(offerGranted > 0 )
						{
							int         discount      = offer.getInt("discount");
							JsonObject  productGet    = getProduct(offer.getString("get"));
							double      price         = productGet.getJsonNumber("price").doubleValue();
							double      accumulateDiscount = 0;
							for(int i =0; i < offerGranted; i++)
							{
								if(basketQuantity>0) {
									double discountValue = price / 100 * discount;
									accumulateDiscount += discountValue;// calculates the discount value
									basketQuantity--; // apply the discount
									offers.append(productGet.getString("name")+": "+ discount+ "% off: -" + formatNumber(discountValue)+"\n");
								}
							}
							total.accumulate(accumulateDiscount);
						}
					}
				});
			}
		});
		
		 if(subtotal.doubleValue() > 0)
		 {
			 System.out.println("Subtotal: "+formatNumber(subtotal.doubleValue()));
			 if(offers.length()>0) { // verify if there are offers
				 System.out.println(offers.toString());
			 }else
			 {
				 System.out.println("(no offers available)");
			 }
			 System.out.println("Total: "+formatNumber(subtotal.doubleValue() - total.doubleValue()));
		      
		 }else
		 {
			 System.out.println("Please, add some product in the basket.");
		 }
		   
	}


	/**
	 * check the value and apply the appropriate pattern 
	 * @param number
	 * @return
	 */
	private static String formatNumber(double number) {
		DecimalFormatSymbols decimal = new DecimalFormatSymbols();
		decimal.setDecimalSeparator('.');
		
		String pattern = "£0.00";
		if(number < 1) {
			number  = number * 100;
			pattern = "##p";
		}
			
		NumberFormat formatter = new DecimalFormat(pattern, decimal);
		String valueFormated = formatter.format(number);
		return valueFormated;
	}
	


	/**
	 * Filter Product by Name
	 * @param productName
	 * @return product filtered by name
	 */
	private static JsonObject getProduct(String productName) {
		Optional<JsonObject> product = products.stream()
        .map(JsonObject.class::cast)
        .filter(obj -> obj.getString("name").equals(productName)).findFirst();
		
		return product.orElse(null);
	}
	
	/**
	 * Filter Offers
	 * @param productName
	 * @return list of offers per product
	 */
	private static List<JsonObject> filterOffers(String productName){
		List<JsonObject> filteredOffers = offers.stream()
        .map(JsonObject.class::cast)
        .filter(obj -> obj.getString("buy").equals(productName)).collect(Collectors.toList());
		
		return filteredOffers;
	}


	/**
	 * Load file of products once application is started
	 */
	private static void loadProducts() {
		File        productFile = new File("products.json");
		try(InputStream fis         = new FileInputStream(productFile)){
			JsonReader reader   = Json.createReader(fis);
			JsonObject jsonFile = reader.readObject();
			products  = (JsonArray) jsonFile.get("products");
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 *  Load file of offers once application is started
	 */
	private static void loadOffers() {
		File        productFile = new File("offers.json");
		try(InputStream fis         = new FileInputStream(productFile)){
			JsonReader reader   = Json.createReader(fis);
			JsonObject jsonFile = reader.readObject();
			offers  = (JsonArray) jsonFile.get("offers");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
